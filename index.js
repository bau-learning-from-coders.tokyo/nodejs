var express = require("express");
var app = express();
var port = 3000;

app.get("/", function(request, response) {
	console.log("Hello Guys");
});

app.get("/users", function(request, response) {
	console.log("This route is for Users");
});
app.listen(port, function() {
	console.log(`Server is listening on port ${port}`);
});
